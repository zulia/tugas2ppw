from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from .models import Perusahaan

response= {}
id_per = ""

# Create your views here.
def index(request):
    html = 'app_profile/session/app_login.html'
    response['status'] = "None"
    print(request);
    return render(request, html, response)

@csrf_exempt
def load_profile(request):
    namaPerusahaan = request.POST['namaPerusahaan']
    headline = request.POST['headline']
    industry = request.POST['industry']
    email = request.POST['email']
    picture = request.POST['picture']
    public = request.POST['public']

    length_perusahaan = Perusahaan.objects.filter(nama_perusahaan = namaPerusahaan).count()
    if length_perusahaan == 0:
        perusahaan = Perusahaan(nama_perusahaan=namaPerusahaan, tipe_perusahaan=industry, web_perusahaan=public, spesialitas_perusahaan=headline, pic_perusahaan=picture)
        perusahaan.save()


    

def refresh(request, id):
    perusahaan = Perusahaan.objects.get(nama_perusahaan= id)
    perusahaan.delete()
    print(perusahaan.nama_perusahaan)
    response = {'name': perusahaan.nama_perusahaan, 'type': perusahaan.tipe_perusahaan, 'web': perusahaan.web_perusahaan, 'spesial': perusahaan.spesialitas_perusahaan, 'pic': perusahaan.pic_perusahaan}
    html = "app_profile/app_profile.html"
    return render(request, html, response)


