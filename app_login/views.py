# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

response = {}

# NOTE : untuk membantu dalam memahami tujuan dari suatu fungsi (def)
# Silahkan jelaskan menggunakan bahasa kalian masing-masing, di bagian atas
# sebelum fungsi tersebut.

# ======================================================================== #
# User Func
# Apa yang dilakukan fungsi INI? #silahkan ganti ini dengan penjelasan kalian 
def index(request):
    print ("#==> masuk index")
    if 'id' in request.session:
        return HttpResponseRedirect(reverse('app-login:profile'))
    else:
        html = 'app_login/app_login.html'
        response['login'] = False
        return render(request, html, response)

def add_session(res, request):
	response['login'] = True
	id = request.POST['id']
	request.session['id'] = id

def profile(request):
    print ("#==> profile")
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('app-login:index'))

    set_data_for_session(response, request)

    html = 'app_login/app_login.html'
    return render(request, html, response)